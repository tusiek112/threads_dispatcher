import time
import datetime
import threading
import queue


def calculate(x):
    time.sleep(x)
    return x*x


def log(message):
    now = datetime.datetime.now().strftime("%H:%M:%S")
    print("{} {}".format(now, message))


class ThreadCalc(threading.Thread):
    def __init__(self, id, jobs_queue):
        threading.Thread.__init__(self, name="ThreadCalc - {}".format(id))
        self.jobs_queue = jobs_queue

    def run(self):
        while True:
            # watek sie blokuje w oczekiwaniu az cos trafi do kolejki
            req = self.jobs_queue.get()
            if req is None:
                # Nie ma nic wiecej do przetwarzania, wiec konczymy
                print("{}-break".format(self.getName()))
                break
            result = calculate(req)
            log("{} -> {}".format(self.getName(), req, result))


def main():
    log("start main thread")
    jobs_queue = queue.Queue()
    n_nr_threads = 3
    for i in range(n_nr_threads):
        ThreadCalc(i, jobs_queue,).start()

    jobs_queue.put(4)
    jobs_queue.put(5)
    jobs_queue.put(3)
    jobs_queue.put(1.5)
    jobs_queue.put(2.2)

    # wysylamy zadania zakonczenia przetwarzania do wszystkich watkow
    for i in range(n_nr_threads):
        jobs_queue.put(None)
    log("end main thread")




main()
