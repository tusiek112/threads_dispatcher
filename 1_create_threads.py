import time
import datetime
import threading


def calculate(x):
    time.sleep(x)
    return x*x


def log(message):
    now = datetime.datetime.now().strftime("%H:%M:%S")
    print("{} {}".format(now, message))


# kazde zadanie obliczeniowe uruchamiane jest w osobnym watku
class ThreadCalc(threading.Thread):
    def __init__(self, value):
        threading.Thread.__init__(self)
        self.value = value

    def run(self):
        result = calculate(self.value)
        log("{} -> {}".format(self.value, result))


def main():
    log("start main thread")
    ThreadCalc(3).start()
    ThreadCalc(2.5).start()
    ThreadCalc(1).start()
    log("end main thread")


if __name__ == "__main__":
    main()

