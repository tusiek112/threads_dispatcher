import time
import datetime
import threading
import queue


task_queue = queue.Queue()


def log_to_stdout(message):
    now = datetime.datetime.now().strftime("%H:%M:%S")
    print("%s %s" % (now, message))


def log_nothing(message):
    pass


local = threading.local()
local.logging = log_to_stdout


def log(message):
    local.logging(message)


def calculate(x):
    log("obliczam %r" % (x,))
    time.sleep(x)
    result = x*x
    log("koncze obliczanie %r; wynik wynosi %r" % (x, result))
    return result


class Task:
    def __init__(self, value, result_queue, debug):
        self.value = value
        self.result_queue = result_queue
        self.debug = debug


# Watki w puli oczekujace na zadania w kolejce "task_queue"
class CalcThread(threading.Thread):
    def __init__(self, id, task_queue):
        threading.Thread.__init__(self, name="CalcThread-{}".format(id))
        self.task_queue = task_queue

    def run(self):
        local.logging = log_nothing
        while True:
            #watek sie blokuje w oczekiwaniu az cos trafi do kolejki
            req = self.task_queue.get()
            if req is None:
                # nie ma nic wiecej do przetwarzania
                print("{}-break".format(self.getName()))
                break
            obj = req
            if obj.debug:
                local.logging = log_to_stdout
            else:
                local.logging = log_nothing
            result = calculate(obj.value)
            log("{} -> {}".format(self.getName(), req.value, result))
            obj.result_queue.put(result)


def threaded_sum(values):
    nsum = 0.0
    result_queue = queue.Queue()
    for value in values:
        debug = (value > 3)
        task = Task(value, result_queue, debug=debug)
        task_queue.put(task)
    for _ in values:
      nsum += result_queue.get()
    return nsum


def main():
    log("uruchamiam watek glowny")
    # inicjujemy pule watkow w trzema watkami "obliczeniowymi"
    N_liczba_watkow = 3
    for i in range(N_liczba_watkow):
        CalcThread(i, task_queue).start()

    #wrzucamy 5 zadan
    result = threaded_sum((4, 5, 3, 1.5, 2.2))
    log("suma wynosi: {:.2f}".format(result))

    # wysylamy zadania zakonczenie przetwarzania
    for i in range(N_liczba_watkow):
        task_queue.put(None)
    log("Koniec watku glownego")


if __name__ == "__main__":
    main()

