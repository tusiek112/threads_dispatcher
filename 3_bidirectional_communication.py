import time
import datetime
import threading
import queue

task_queue = queue.Queue()


def log(message):
    now = datetime.datetime.now().strftime("%H:%M:%S")
    print("{} {}".format(now, message))


def calculate(x):
    time.sleep(x)
    return x*x


class Task:
    def __init__(self, value, result_queue):
        self.value = value
        self.result_queue = result_queue


# Watki w puli oczekujace na zadania w kolejce "task_queue"
class CalcThread(threading.Thread):
    def __init__(self, id, task_queue):
        threading.Thread.__init__(self, name= "CalcThread-{}".format(id))
        self.task_queue  = task_queue
    def run(self):
        while True:
            #watek sie blokuje w oczekiwaniu az cos trafi do kolejki
            req = self.task_queue.get()
            if req is None:
                # nie ma nic wiecej do przetwarzania
                print("{}-break".format(self.getName()))
                break
            obj = req
            result = calculate(obj.value)
            log("{} -> {}".format(self.getName(), req.value, result))
            obj.result_queue.put(result)


def threaded_sum(values):
    nsum = 0.0
    result_queue = queue.Queue()
    for value in values:
        task = Task(value, result_queue)
        task_queue.put(task)
    for _ in values:
      nsum += result_queue.get()
    return nsum


def main():
    log("uruchamiam watek glowny")
    # inicjujemy pule watkow w trzema watkami "obliczeniowymi"
    N_liczba_watkow = 3
    for i in range(N_liczba_watkow):
        CalcThread(i, task_queue).start()

    #wrzucamy 5 zadan
    result = threaded_sum((4, 5, 3, 1.5, 2.2))
    log("suma wynosi: {:.2f}".format(result))

    # wysylamy zadania zakonczenie przetwarzania
    for i in range(N_liczba_watkow):
        task_queue.put(None)
    log("Koniec watku glownego")


if __name__ == "__main__":
    main()