# https://stackoverflow.com/questions/16745507/tkinter-how-to-use-threads-to-preventing-main-event-loop-from-freezing
"""

When you join the new thread in the main thread, it will wait until the thread finishes, so the GUI will block even
though you are using multithreading.

If you want to place the logic portion in a different class, you can subclass Thread directly, and then start a new
object of this class when you press the button. The constructor of this subclass of Thread can receive a Queue object
and then you will be able to communicate it with the GUI part. So my suggestion is:

Create a Queue object in the main thread
Create a new thread with access to that queue
Check periodically the queue in the main thread
Then you have to solve the problem of what happens if the user clicks two times the same button (it will spawn a
new thread with each click), but you can fix it by disabling the start button and enabling it again after you call
self.prog_bar.stop().

"""

import Queue

class GUI:
    # ...

    def tb_click(self):
        self.progress()
        self.prog_bar.start()
        self.queue = Queue.Queue()
        ThreadedTask(self.queue).start()
        self.master.after(100, self.process_queue)

    def process_queue(self):
        try:
            msg = self.queue.get(0)
            # Show result of the task if needed
            self.prog_bar.stop()
        except Queue.Empty:
            self.master.after(100, self.process_queue)

class ThreadedTask(threading.Thread):
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue
    def run(self):
        time.sleep(5)  # Simulate long running process
        self.queue.put("Task finished")